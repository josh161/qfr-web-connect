<?php
require_once __DIR__ . '/vendor/autoload.php';
use Jenssegers\Blade\Blade;

# views is the root path for blade files, cache is a cache directory
$blade = new Blade('views','cache');

$filename="";
if (isset($argv[1])) {
	$filename=$argv[1];
}
else {
	echo "Error: no file specified\n";
	exit(1);
}

if (isset($argv[2])) {
	$layoutoverride=$argv[2];
}

$jsonloader=file_get_contents($filename);

$invars=json_decode($jsonloader,true);


foreach ($invars as $tk => $tv) {

    if ($tk == "team") {
        makeit($outvar["team"]); 
        foreach ($invars["team"] as $ik => $iv) {
            $outvar["team"]->{$ik}=$iv;
        }
    }
    elseif ($tk=="post") {
        makeit($outvar["post"]); 
        foreach ($invars["post"] as $bik => $biv) {
            if ($bik=="settings") {
                makeit($outvar["post"]->{"settings"});
                #makeit($outvar["post"]->{"settings"}->{"gallery"});
                foreach ($invars["post"]["settings"] as $ok1 => $ov1) {
                    $i=0;
                    if ($ok1 == "gallery") {
                        foreach ($invars["post"]["settings"]["gallery"] as $thisitem) {
                            foreach ($thisitem as $ik => $iv) {
                                makeit($outvar["post"]->{"settings"}->{"gallery"}[$i]);
                                $outvar["post"]->{"settings"}->{"gallery"}[$i]->{$ik}=$iv;
                            }
                            $i++;
                        }
                    }
                    else {
                        $outvar["post"]->{"settings"}->{$ok1}=$ov1;
                    }
                }
                    
            }
            else {
                $outvar["post"]->{$bik}=$biv;
            }
        }
    }
    elseif ($tk=="news") {
        $i=0;
        foreach ($invars["news"] as $thisitem) {
            foreach ($thisitem as $ik => $iv) {
		    makeit($outvar["news"][$i]);
		    $outvar["news"][$i]->{$ik}=$iv;
            }
    
            $i++;

        }
    }

    elseif ($tk=="website") {
        foreach ($invars["website"] as $ik => $iv) {
   		makeit($outvar["website"]); 
            $outvar["website"]->{$ik}=$iv;
        }
    }
    else {
        $outvar[$tk]=$tv;
    }

}

if (!isset($outvar["same_as"])) {
	$outvar["same_as"]="";
}

# if you want to override the layout, you can do it here:
if (isset($layoutoverride)) {
	$outvar["layout"]=$layoutoverride;
}

echo $blade->render('templates.website.page', $outvar);

# makes some templates work
function asset($invar) {
    return $invar;
}

# gets rid of annoying warnings
function makeit(&$invar) {

    if (!isset($invar))
    $invar = new stdClass();

    return $invar;

}
?>

