# qfr-web-connect

TO INSTALL: 

        composer require jenssegers/blade
        mkdir cache; mkdir views/templates -p
        cd views/templates && ln -s ../../website .

TO RUN:

	bash runtests.sh

There will be html files generated in the tests directory.


TO CREATE A NEW TEMPLATE:

1. Add the new template blade file into the website/custom/ directory 
   named as <name>.blade.php

2. Run the test:
	bash runtests.sh <layout_name>


To integrate a permanent test scenario using the layout create a json file
that uses the new layout name as the root layout variable. This will
then run by default when no layout_name is specified.


MORE INFO:
More info on installed composer software:
https://github.com/jenssegers/blade


