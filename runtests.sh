#!/bin/bash

if [[ "$1" == "--help" ]]; then
	echo "USAGE: $0 [<layoutoverride>|--help]"
	echo "
	first argument:

	--help	displays this message
	--all	runs all of the templates to make sure they finish

	otherwise the first argument is assumed 
	to override the layout variable in all tests

	"

fi

if [[ "$1" == "--all" ]]; then

	for n in `cd website/custom && ls *.blade.php | cut -f1 -d.`; do
		echo $n
		bash runtests.sh $n
	done
	exit 0

fi


if [ ! -z "$1" ]; then
	layoutoverride="$1"
fi

if [ -z "$layoutoverride" ]; then
	for n in `ls tests/*json`; do
		php generate.php $n > $n.html
	done
else
	for n in `ls tests/*json`; do
		php generate.php $n $layoutoverride > $n.html
	done
fi	
