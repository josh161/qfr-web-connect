@extends('templates.website.general.root')
{{-- live reference: https://s3-us-west-2.amazonaws.com/jdat/_public/templates/web/orly-bootstrap-coming-soon-template/index.html --}}
@section('template_css')

@section('template_css')

<link rel="stylesheet" href="https://s3-us-west-2.amazonaws.com/jdat/_public/templates/web/orly-bootstrap-coming-soon-template/css/bootstrap.min.css">
<link rel="stylesheet" href="https://s3-us-west-2.amazonaws.com/jdat/_public/templates/web/orly-bootstrap-coming-soon-template/css/main.css">
<link rel="stylesheet" href="https://s3-us-west-2.amazonaws.com/jdat/_public/templates/web/orly-bootstrap-coming-soon-template/css/responsive.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

@endsection
@section('template_body')

<body>

<!--header section -->
<section class="banner" role="banner">
  <div class="banner-area"> 
    <!-- overlay -->
    <div class="banner-area-gradient"></div>
    <!-- overlay -->
    <div class="inner-bg">
      <div class="container">
        <div class="col-md-10 col-md-offset-1">
          <div class="banner-text text-center"> 
                                    @if (!empty($logo))
                                        <a  href="{{ $home_url }}">
                                        <img class="logo" style="max-height:100px" src="{{ $logo }}" alt="{{ $site_name }}">    
                                        </a>                                    
                                    @else
                                        <a class="logo" href="{{ $home_url }}">
                                        {{ $site_name }}
                                        </a>
                                    @endif	
            <h1>Coming soon</h1>
            <!--Countdown -->
            <div id="countdown"></div>
            <!--Countdown --> 
            <a href="#subscribes" class="btn">Notify me</a>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--header section --> 
<!--intro section -->



@if(!empty($post->image) || !empty($post->content) || (isset($bread) && $bread == true))
<!--intro section -->
<section id="intro" class="section intro">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="intro-content">
			@if(!empty($post->image))
			<div align="center"><img class="img-responsive" src="{{ $post->image }}" alt="{{ $post->title ?? '' }}"></div>
			@endif
            @if((isset($bread) && $bread == true))
          <h1>{{ $post->title ?? '' }}</h1>
			@if(!empty($post->date))<ul class="square">
				<li>{{ $post->date ?? '' }}</li>
			</ul>@endif
            @endif
        </div>
        
        <!-- intro --> 
      </div>
      <div class="col-md-12 col-sm-12">
        <div class="intro-content">
				@if(!empty($post->content))<div id="connn">
                    <style>div#connn label.label {color:black;}</style>
					{!! $post->content !!}</div>
				@endif
        </div>
        
        <!-- intro --> 
      </div>
    </div>
  </div>
</section>
<!--intro section --> 
@endif



<!--intro section --> 
<!--subscribe section -->
<section id="subscribes" class="subscribe">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-5 subscribe-title">
        <h2>Sign Up for our Newsletter!</h2>
      </div>
      
      <!-- subscribe form -->
      <div class="col-sm-12 col-md-7 subscribe-form"> 
               
      <style>
        div.subscribe-form label.label {color:white; font-size:larger;}
        div.subscribe-form input {font-size:larger; }

        </style>
        {!! $sign_up !!}
        
        <!-- subscribe message -->
        <div id="mesaj"></div>
        <!-- subscribe message --> 
      </div>
      <!-- subscribe form --> 
    </div>
  </div>
</section>
<!--subscribe section --> 

<!-- Footer section -->
<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="footer-col col-md-5">
      <p>Copyright © {{ $year }} All Right Reserved. </P>
          @if($website->powered_by)
                          <P> 
                          Design by <a target="_blank" href="http://www.designstub.com/">Designstub</a>.
                          Powered by <a href="https://www.dataczar.com/" target="_blank" >Dataczar</a>.
                          </p>
          @endif
      </div>
      <div class="footer-col col-md-7">
        <ul class="footer-share">
                        @if(!empty($team->facebook))<li style="display:inline;"><a target="_blank" href="{{ $team->facebook}}"><i class="fa fa-facebook"></i></a></li>@endif
                        @if(!empty($team->twitter))<li style="display:inline;"><a target="_blank"  href="{{ $team->twitter}}"><i class="fa fa-twitter"></i></a></li>@endif
                        @if(!empty($team->linkedin))<li style="display:inline;"> <a target="_blank"  href="{{ $team->linkedin}}"><i class="fa fa-linkedin"></i></a></li>@endif
                        @if(!empty($team->instagram))<li style="display:inline;"> <a target="_blank"  href="{{ $team->instagram}}"><i class="fa fa-instagram"></i></a></li>@endif
                        @if(!empty($team->yelp))<li style="display:inline;"><a target="_blank"  href="{{ $team->yelp}}"><i class="fa fa-yelp"></i></a></li>@endif
                        @if(!empty($team->youtube))<li style="display:inline;"><a target="_blank"  href="{{ $team->youtube}}"><i class="fa fa-youtube"></i></a></li>@endif
        </ul>
      </div>
    </div>
  </div>
  
  <!-- footer top --> 
  
</footer>
<!-- Footer section --> 

<!-- JS files--> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<script src="https://s3-us-west-2.amazonaws.com/jdat/_public/templates/web/orly-bootstrap-coming-soon-template/js/bootstrap.min.js"></script> 
<script src="https://s3-us-west-2.amazonaws.com/jdat/_public/templates/web/orly-bootstrap-coming-soon-template/js/jquery.backstretch.min.js"></script> 
<script src="https://s3-us-west-2.amazonaws.com/jdat/_public/templates/web/orly-bootstrap-coming-soon-template/js/jquery.countdown.js"></script> 
<script type="text/javascript" src="https://s3-us-west-2.amazonaws.com/jdat/_public/templates/web/orly-bootstrap-coming-soon-template/js/jquery.subscribe.js"></script> 
<script>

jQuery(document).ready(function() {	
	
    /*
        Background slideshow
    */
	$('.banner-area').backstretch([
        @if(isset($post->settings->gallery_status) && $post->settings->gallery_status == 'enabled' && isset($post->settings->gallery)
               && count($post->settings->gallery) > 0)
               @foreach($post->settings->gallery as $gitem)
               "{{$gitem->url ?? 'https://s3-us-west-2.amazonaws.com/jdat/_public/templates/web/orly-bootstrap-coming-soon-template/images/backgrounds/1.jpg'}}",
                @endforeach
        @else
	                     "https://s3-us-west-2.amazonaws.com/jdat/_public/templates/web/orly-bootstrap-coming-soon-template/images/backgrounds/1.jpg"
	                   , "https://s3-us-west-2.amazonaws.com/jdat/_public/templates/web/orly-bootstrap-coming-soon-template/images/backgrounds/2.jpg"
	                   , "https://s3-us-west-2.amazonaws.com/jdat/_public/templates/web/orly-bootstrap-coming-soon-template/images/backgrounds/3.jpg"
        @endif
	                  ], {duration: 3000, fade: 750});
					  
    
  

});

	// cowntdown function. Set the date by modifying the date in next line (June 1, 2012 00:00:00):
	var austDay = new Date("{{$post->start_time ?? ''}}");
		$('#countdown').countdown({until: austDay, layout: '<div class="item"><p>{dn}</p> {dl}</div> <div class="item"><p>{hn}</p> {hl}</div> <div class="item"><p>{mn}</p> {ml}</div> <div class="item"><p>{sn}</p> {sl}</div>'});
		$('#year').text(austDay.getFullYear());
		
	// smooth scrolling	
		$(function() {
	  $('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top
	        }, 1000);
	        return false;
	      }
	    }
	  });
	});
	</script>
</body>
@endsection

