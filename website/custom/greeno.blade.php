@extends('templates.website.general.root')
{{-- live reference: https://trk.dzr.io/webtemplates/greeno/index.html --}}

@section('template_css')
       <!-- bootstrap css -->
      <link rel="stylesheet" href="https://trk.dzr.io/webtemplates/greeno/css/bootstrap.min.css">
      <!-- style css -->
      <link rel="stylesheet" href="https://trk.dzr.io/webtemplates/greeno/css/style.css">
      <!-- Responsive-->
      <link rel="stylesheet" href="https://trk.dzr.io/webtemplates/greeno/css/responsive.css">
      <!-- Scrollbar Custom CSS -->
      <link rel="stylesheet" href="https://trk.dzr.io/webtemplates/greeno/css/jquery.mCustomScrollbar.min.css">
      <!-- Tweaks for older IEs-->
      <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
@endsection

@section('template_body')
   <!-- body -->
   <body class="main-layout">
      <!-- loader  -->
      <!--
      <div class="loader_bg">
         <div class="loader"><img src="https://trk.dzr.io/webtemplates/greeno/images/loading.gif" alt="#" /></div>
      </div>
-->
      <!-- end loader -->
      <!-- header -->
      <header>
         <!-- header inner -->
         <div class="header">
            <div class="container">
               <div class="row">
                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col logo_section">
                     <div class="full">
                        <div class="center-desk">
                           <div class="logo">
                           <a href="{{ $home_url }}">
                              @if (!empty($logo))
                                 <img style="max-height:100px" src="{{ $logo }}" alt="{{ $site_name }}">
                              @else
                                 <h1><b>{{ $site_name }}</b></h1>
                              @endif
                           </a> 
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9">
                     <div class="menu-area">
                        <div class="limit-box">
                           <nav class="main-menu">
                              <ul class="menu-area-main">
                              @foreach ($menu as $item)    
                                 <li {!! strpos($item['class'],'is-active') ? 'class="active"' : $item['class'] !!}>
                                    <a {!! strpos($item['link'], 'http' )===false ? '' : 'target="_blank"' !!} href="{{ $item['link'] }}">
                                       {{ $item['title'] }}
                                    </a>
                                 </li>
                              @endforeach 
                              @if(!empty($trunc))
                                 @foreach ($trunc as $item)
                                 <li {!! strpos($item['class'],'is-active') ? 'class="active"' : $item['class'] !!}>
                                    <a {!! strpos($item['link'], 'http' )===false ? '' : 'target="_blank"' !!} class="navbar-item{{ isset($item['class']) ? $item['class'] : '' }}" href="{{ $item['link'] }}">
                                       {{ $item['title'] }}
                                    </a>
                                 </li>
                                 @endforeach
                              @endif
                              </ul>
                           </nav>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- end header inner -->
      </header>
      <!-- end header -->
   @if(isset($post->settings->gallery_status) && $post->settings->gallery_status == 'enabled' && isset($post->settings->gallery)
               && count($post->settings->gallery) > 0)
      <section >
         <div id="main_slider" class="carousel slide banner-main" data-ride="carousel" style="background: url('{{ $bg_image ?? 'https://dataczar-public.s3.us-west-2.amazonaws.com/photos/54/51e6dc474f53b108f5d084609621327f1038dae54e507748752b73dc9e4fc0_1280.jpg' }}');"}}>
           <!-- <ol class="carousel-indicators">
               <li data-target="#main_slider" data-slide-to="0" class="active"></li>
               <li data-target="#main_slider" data-slide-to="1"></li>
               <li data-target="#main_slider" data-slide-to="2"></li>
            </ol> -->
            <div class="carousel-inner">
            @foreach($post->settings->gallery as $gitem)
               <div class="carousel-item {{ $loop->iteration == 1 ? 'active' : ''  }}">
                  <div class="container">
                     <div class="row marginii">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                           <div class="carousel-caption ">
                              <h1 style="word-break:break-word;">{{$gitem->title ?? ''}}</h1>
                              <p>{{$gitem->description ?? ''}}</p>
                              @if(isset($gitem->permalink))
                                 <a class="btn btn-lg btn-primary" href="{{$gitem->permalink ?? ''}}" role="button">{{$gitem->button ?? 'Read More'}}</a>
                              @endif
                           </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                           <div class="img-box">
                              <figure><img src="{{$gitem->url ?? ''}}" alt="{{$gitem->title ?? ''}}"/></figure>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            @endforeach
            </div>
            @if(count($post->settings->gallery)>1)
               <a class="carousel-control-prev" href="#main_slider" role="button" data-slide="prev">
               <i class='fa fa-angle-left'></i></a>
               <a class="carousel-control-next" href="#main_slider" role="button" data-slide="next">
               <i class='fa fa-angle-right'></i>
            @endif
            </a>
         </div>
      </section>
   @endif
      <!-- plant -->
      <P>&nbsp</p>
      <P>&nbsp</p>
      <P>&nbsp</p>
<style>
.plants form {
   background: unset;
}
</style>
      <div id="plant" class="plants">
         <div class="container">
            <div class="row">
               <div class="col-md-12 ">
                  <div class="titlepage">
                  @if(!empty($post->image))
                     <figure><img src="{{ $post->image }}" alt="img"/></figure>
                  @endif
                  @if(isset($bread) && $bread == true)
                     <h2>{{ $post->title ?? '' }}</h2>
                  @endif
                  @if(!empty($post->date))
                  <span>
                     {{ $post->date ?? '' }}
                  </span>
                  @endif
                  </div>
                  @if(!empty($post->content))
                  <span>
                        <!-- start content -->
                           {!! $post->content !!}
                        <!-- end content -->
                  </span>
                  @endif
               </div>
            </div>
         </div>
      </div>
      <!-- end plant -->

      {{-- Posts loop --}}
		<!-- LOOP -->
		@if(!empty($news))
      <div id="plant" class="plants">
         <div class="container">
            <div class="row">
            @foreach ($news as $item)
               <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                  <div class="plants-box">
                     <a href="{{ $item->permalink ?? '' }}"  role="button"  {{ !empty($item->external) ? 'target="_blank"' : '' }} rel="noopener">
                        @if(!empty($item->image))
                           <figure><img style="max-height:400px;width:unset;" src="{{ $item->image }}" alt="img"/></figure>
                        @endif
                        <h3 class="title"> {{ $item->title ?? '' }}</h3>
                     </a>
                     @if(!empty($item->date))
                        <P><span>{{ $item->date ?? '' }}</span></<P>
                    @endif
                     @if(!empty($item->description))
                        <p>{{ $item->description ?? '' }}</p>
                     @endif
                     <a class="btn btn-lg btn-primary"  href="{{ $item->permalink ?? '' }}"  role="button"  {{ !empty($item->external) ? 'target="_blank"' : '' }} rel="noopener">
                        Read More
                     </a>

                  </div>
               </div>
            @endforeach
            </div>
         </div>
      </div>
      @endif

      @if (isset($pages) && $pages > 1)
			<div class="columns">
			    <div class="column is-4 is-offset-4">
				<hr>
				<nav class="pagination is-rounded" role="navigation" aria-label="pagination">
				    @if ($previous === NULL)
				    <a class="pagination-previous" disabled>Previous</a>
				    @else
				    <a class="pagination-previous" href="{{ $previous }}">Previous</a>
				    @endif
				    @if ($next === NULL)
				    <a class="pagination-next" disabled>Next page</a>
				    @else
				    <a class="pagination-next" href="{{ $next }}">Next page</a>
				    @endif
				</nav>
			    </div>
			</div>
			@endif

      <!--contact -->
      @if(!empty($sign_up) && $website->newsletter_signup == true)
      @if(empty($thanks))

      <div id="contact" class="contact">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="titlepage">
                     <h2>Sign up for our Newsletter!</h2>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 paddimg-right">
                  <div class="row">
                     <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                              <h5 class="title">{!! $sign_up !!}</h5>
                     </div>
                     <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                     @if(!empty($map))   
                        <div class="map_section">
                           <div class="mapouter">
                              <div class="gmap_canvas">
                                 <iframe width="100%" height="250" id="gmap_canvas" src="https://maps.google.com/maps?q={{ $map }}&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                              </div>
                              <style>.mapouter{text-align:right;height:250px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:250px;width:100%;}</style>
                           </div>
                        </div>
                     @endif
                     </div>
                  </div>
               </div>
               
               <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 paddimg-left">
                  <div class="Nursery-img">
                     <figure>
                        @if( !empty($bg_image)) 
                           <img src="{{ $bg_image }}" alt="img"/>
                        @else
                           <!--<img src="https://trk.dzr.io/webtemplates/greeno/images/contactimg.jpg" alt="img"/>-->
                           <img src="https://dataczar-public.s3.us-west-2.amazonaws.com/photos/54/51e6dc474f53b108f5d084609621327f1038dae54e507748752b73dc9e4fc0_1280.jpg" alt="img"/>
                        @endif
                        <div class="text-box">
                           <h3>{{ $site_name }}</h3>
                        </div>
                     </figure>
                  </div>
               </div>
               
            </div>
         </div>
      </div>
      @endif

      @endif
      <!-- end contact -->
      <!-- footer -->
      <footer>
         <div id="contact" class="footer">
            <div class="container">
               <div class="row pdn-top-30">
                  <div class="col-md-12 ">
                     <div class="footer-box">
                        <div class="headinga">
                           <h3>Address</h3>
                           <span>
                              {{ $site_name }}<BR>
                              @if(!empty($team->address)){{ $team->address }}<BR>@endif
                              @if(!empty($team->address_2)){{ $team->address_2 }}<BR>@endif
                              @if(!empty($team->city)){{ $team->city }}, {{ $team->state }} {{ $team->zip }}@endif
                           </span>
                           <p>
                              @if(!empty($team->phone))( {{ $team->phone }} )@endif
                           </p>
                        </div>
                        <ul class="location_icon">
                           @if(!empty($team->facebook))<li><a href="{{ $team->facebook}}" target="_blank"><img src="https://trk.dzr.io/webtemplates/greeno/icon/facebook.png"></a></li>@endif
                           @if(!empty($team->twitter))<li><a href="{{ $team->twitter}}" target="_blank"><img src="https://trk.dzr.io/webtemplates/greeno/icon/Twitter.png"></a></li>@endif
                           @if(!empty($team->linkedin))<li> <a href="{{ $team->linkedin}}" target="_blank"><img src="https://trk.dzr.io/webtemplates/greeno/icon/linkedin.png"></a></li>@endif
                           @if(!empty($team->instagram))<li> <a href="{{ $team->instagram}}" target="_blank"><img src="https://trk.dzr.io/webtemplates/greeno/icon/instagram.png"></a></li>@endif
                           @if(!empty($team->yelp))<li><a style="color: #eeeeee;" href="{{ $team->yelp}}" target="_blank">Yelp</a></li>@endif
                           @if(!empty($team->youtube))<li><a style="color: #eeeeee;" href="{{ $team->youtube}}" target="_blank">YouTube</a></li>@endif
                        </ul>
                        <div class="menu-bottom">
                        @if(!empty($footer['menu_items']))
                           <ul class="link">
                              @foreach ($footer['menu_items'] as $item)
                              <li><a href="{{ $item['link'] }}" {!! strpos($item['link'], 'http') === false ? '' : 'target="_blank"' !!}>{{ $item['title'] }}</a></li>
                              @endforeach
                           </ul>
                        @endif
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="copyright">
               <div class="container">
                  <p>Copyright {{ $year }} All Rights Reserved.
                  @if($website->powered_by)
                            Design by <a href="https://html.design" target="_blank">html.design</a>.
                            Powered By <a href="https://www.dataczar.com/" target="_blank" >Dataczar</a>.
                  @endif
               </p>
               </div>
            </div>
         </div>
      </footer>
      <!-- end footer -->
      <!-- Javascript files-->
      <script src="https://trk.dzr.io/webtemplates/greeno/js/jquery.min.js"></script>
      <script src="https://trk.dzr.io/webtemplates/greeno/js/popper.min.js"></script>
      <script src="https://trk.dzr.io/webtemplates/greeno/js/bootstrap.bundle.min.js"></script>
      <script src="https://trk.dzr.io/webtemplates/greeno/js/jquery-3.0.0.min.js"></script>
      <script src="https://trk.dzr.io/webtemplates/greeno/js/plugin.js"></script>
      <!-- sidebar -->
      <script src="https://trk.dzr.io/webtemplates/greeno/js/jquery.mCustomScrollbar.concat.min.js"></script>
      <script src="https://trk.dzr.io/webtemplates/greeno/js/custom.js"></script>
      <!-- javascript --> 
      <script src="https://trk.dzr.io/webtemplates/greeno/js/owl.carousel.js"></script>
      <script src="https:cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
      <script>
         $(document).ready(function(){
         $(".fancybox").fancybox({
         openEffect: "none",
         closeEffect: "none"
         });
         
         $(".zoom").hover(function(){
         
         $(this).addClass('transition');
         }, function(){
         
         $(this).removeClass('transition');
         });
         });
         
      </script> 
   </body>
@endsection
