@extends('templates.website.general.root')

@section('template_css')
	@includeif('templates.website.general._gallery_css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.8.1/css/bulma.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
	<style type="text/css">
    .center {
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .card {
        margin-bottom: 35px;
        box-shadow: none;
    }

    body,
    html {
        height: 100%;
        background-color: {{ !empty($bg_color) ? $bg_color : '' }};
        @if( !empty($bg_image)) 
        background-image: url('{{ $bg_image }}');
        background-size: cover;
        background-attachment: fixed;
        @endif
    }

    body {
        display: flex;
        flex-direction: column;
    }

    .breadcrumb li {
        white-space: pre-wrap;
    }

    .main_content {
        flex: 1 0 auto;
    }

    .footer {
        flex: 1 0 auto;
    }

    .navbar {
        box-shadow: 0 2px 0 0 rgba(0, 0, 0, 0.35);
    }

    @if($transparent) .navbar {
        background-color: unset;
        box-shadow: unset;
    }

    @endif 
    @if( !empty($nav_color_override)) 
    .navbar.navbar-color-override, .footer.navbar-color-override {
        background-color: {{ $nav_color_override }};
    }
    @endif 
    p {
        margin-bottom: 1rem;
    }

    .featured {
        min-height: 416px;
        background-repeat: no-repeat;
        background-attachment: inherit;
        background-size: contain;
        background-position: center;
    }

    strong {
        color: unset;
    }

    .content h1,
    .content h2,
    .content h3,
    .content h4,
    .content h5,
    .content h6 {
        color: unset;
    }

    .content {
        word-break: break-word;
    }

    .card-image {
        padding-top: 15px;
    }

    .truncated_menu {
        display: none;
    }

    .truncated_menu_drop {
        display: flex;
    }

    @media only screen and (max-width: 1023px) {
        .truncated_menu {
            display: block;
        }

        .truncated_menu_drop {
            display: none;
        }
    }

    @media only screen and (max-width: 768px) {
        .media-content {
            overflow: visible;
        }

        .featured {
            min-height: 208px;
        }
    }

    .fa-btn {
        padding-right: 10px;
    }

	{{-- support old version --}}
	@if($website->template_id > 126) 
	.navbar-item img {
			max-height: 6.75rem;
			max-height: 90px;
		}

		@media (max-width: 768px) {
			.navbar-item img {
				max-height: 65px;
			}
		}
	@endif
</style>
@endsection

@section('template_body')
<body class="has-navbar-fixed-top" cz-shortcut-listen="true">
        <div class="main_content">
			<nav class="navbar is-fixed-top {{ !empty($nav_color) ? $nav_color : 'is_info' }} navbar-color-override" role="navigation"
		aria-label="main navigation">
				<div class="container">
					<div class="navbar-brand">
						<a class="navbar-item" href="{{ $home_url }}">
							@if (!empty($logo))
								<img src="{{ $logo }}" alt="{{ $site_name }}">
							@else
								{{ $site_name }}
							@endif
						</a>
						<a role="button" class="navbar-burger" data-target="navMenu" aria-label="menu" aria-expanded="false">
							<span aria-hidden="true"></span>
							<span aria-hidden="true"></span>
							<span aria-hidden="true"></span>
						</a>
					</div>
					<div class="navbar-menu" id="navMenu">
						<div class="navbar-end">
							@foreach ($menu as $item)    
								<a {!! strpos($item['link'], 'http') === false ? '' : 'target="_blank"' !!} class="navbar-item{{ isset($item['class']) ? $item['class'] : '' }}" href="{{ $item['link'] }}">
									{{ $item['title'] }} 
								</a>
							@endforeach 
							
							@if(!empty($trunc))
								<div class="truncated_menu">
									@foreach ($trunc as $item)
									<a {!! strpos($item['link'], 'http' )===false ? '' : 'target="_blank"' !!}
										class="navbar-item{{ isset($item['class']) ? $item['class'] : '' }}" href="{{ $item['link'] }}">
										{{ $item['title'] }}
									</a>
									@endforeach
								</div>
								<div class="navbar-item has-dropdown is-hoverable truncated_menu_drop">
									<a class="navbar-link">
										More
									</a>
									<div class="navbar-dropdown">
										@foreach ($trunc as $item)
										<a {!! strpos($item['link'], 'http' )===false ? '' : 'target="_blank"' !!}
											class="navbar-item{{ isset($item['class']) ? $item['class'] : '' }}" href="{{ $item['link'] }}">
											{{ $item['title'] }}
										</a>
										@endforeach
									</div>
								</div>
							@endif



							@if (!empty($team->facebook))<a title="Facebook" class="navbar-item" href="{{ $team->facebook }}" target="_blank" class="button is-rounded"><i class="fa fa-facebook"></i></a>@endif
							@if (!empty($team->twitter))<a title="Twitter" class="navbar-item" href="{{ $team->twitter }}" target="_blank" class="button is-rounded"><i class="fa fa-twitter"></i></a>@endif
							@if (!empty($team->instagram))<a title="instagram" class="navbar-item" href="{{ $team->instagram }}" target="_blank" class="button is-rounded"><i class="fa fa-instagram"></i></a>@endif
							@if (!empty($team->youtube))<a title="youtube" class="navbar-item" href="{{ $team->youtube }}" target="_blank" class="button is-rounded"><i class="fa fa-youtube"></i></a>@endif
							@if (!empty($team->linkedin))<a title="linkedin" class="navbar-item" href="{{ $team->linkedin }}" target="_blank" class="button is-rounded"><i class="fa fa-linkedin"></i></a>@endif
							@if (!empty($team->yelp))<a title="yelp" class="navbar-item" href="{{ $team->yelp }}" target="_blank" class="button is-rounded"><i class="fa fa-yelp"></i></a>@endif
						</div>
					</div>
				</div>
			</nav>
		    <section class="section">

		<div class="columns">
		    <div class="column is-8 is-offset-2">
			@if(isset($bread) && $bread == true)
			<div class="card">
			    <div class="card-content">
				<nav class="breadcrumb" aria-label="breadcrumbs">
				    <ul>
					<li>
					    <a href="{{ $home_url }}"> Home</a>
					</li>
					<li class="is-active"><a href="#">{{ $post->title ?? '' }}</a></li>
				    </ul>
				</nav>
			    </div>
			</div>
			@endif {{-- end bread --}}

			@if(!empty($post->content) ||!empty($post->image) || (isset($post->settings->gallery_status) && $post->settings->gallery_status == 'enabled'))
			<div class="card">
                @includeif('templates.website.general._gallery')
			    @if(!empty($post->image))
			    <div class="card-image">
				<figure class="image is-16x9 featured" style="background-image: url('{{ $post->image }}');">
				    &nbsp;
				</figure>
			    </div>
			    @endif
			    @if(!empty($post->content))
			    <div class="card-content content">
				@if(isset($bread) && $bread == true)
				<div class="media">
				    <div class="media-content">
					<h1 class="title is-2">{{ $post->title ?? '' }}</h1>
					@if(!empty($post->date))
					<h2 class="subtitle">
					    <span class="fa fa-clock-o">&nbsp;</span>
					    <small> {{ $post->date ?? '' }}</small>
					</h2>
					@endif
				    </div>
				</div>
				@endif

				<p>{!! $post->content !!}</p>

				@if(isset($bread) && $bread == true)
				<hr>
				<div class="has-text-centered">
				    <a class="button is-link" href="#"
					onclick="window.open('https://www.facebook.com/sharer.php?u={{ $base_url }}{{ $permalink }}','Share This','menubar=no,toolbar=no,resizable=no,scrollbars=no, width=600,height=455');">
					<i class="fa fa-btn fa-facebook"></i>
					Share
				    </a>
				    <a class="button is-info" href="#"
					onclick="window.open('http://twitter.com/share?text={{ $post->title ?? '' }}&amp;url={{ $base_url }}{{ $permalink }}', 'Post this On twitter', 'menubar=no,toolbar=no,resizable=no,scrollbars=no,width=600,height=455');"
					class="btn" id="b">
					<i class="fa fa-btn fa-twitter"></i>
					Tweet
				    </a>
				</div>

				@endif

			    </div>
			    @endif
			</div> {{-- end page card --}}

			@endif


			{{-- Posts loop --}}
			<!-- LOOP -->
			@if(!empty($news))
			@foreach ($news as $item)

			<div class="card">
			    <div class="card-image">
				@if(!empty($item->image))
				<a href="{{ $item->permalink }}" {{ !empty($item->external) ? 'target="_blank"' : '' }} rel="noopener">
				    <figure class="image is-16x9 featured" style="background-image: url('{{ $item->image }}');">
					&nbsp;
				    </figure>
				</a>
				@endif
			    </div>
			    <div class="card-content">
				<div class="media">
				    <div class="media-content">
					<h1 class="title is-2">{{ $item->title ?? '' }}</h1>
					<h2 class="subtitle">
					    <span class="fa fa-clock-o">&nbsp;</span>
					    <small> {{ $item->date ?? '' }}</small>
					</h2>
				    </div>
				</div>
				<div class="content">
				    @if(!empty($item->description))
				    <p>{{ $item->description ?? '' }}</p>
				    @endif
				    <a href="{{ $item->permalink ?? '' }}" {{ !empty($item->external) ? 'target="_blank"' : '' }} 
						rel="noopener"> Read 
						More >>  </a>
				</div>
			    </div>
			</div>
			@endforeach
			@endif

			@if (isset($pages) && $pages > 1)
			<div class="columns">
			    <div class="column is-4 is-offset-4">
				<hr>
				<nav class="pagination is-rounded" role="navigation" aria-label="pagination">
				    @if ($previous === NULL)
				    <a class="pagination-previous" disabled>Previous</a>
				    @else
				    <a class="pagination-previous" href="{{ $previous }}">Previous</a>
				    @endif
				    @if ($next === NULL)
				    <a class="pagination-next" disabled>Next page</a>
				    @else
				    <a class="pagination-next" href="{{ $next }}">Next page</a>
				    @endif
				</nav>
			    </div>
			</div>
			@endif

	    </section>
	</div>
    
	<div class="footer hero {{ !empty($nav_color) ? $nav_color : 'is_info'  }} navbar-color-override">
    <div class="container">
        <div class="columns is-centered">
            @if(!empty($team->use_public))
            <!-- Column -->
            <div class="column has-text-centered">
                <h1 class="title">Contact</h1>
                <p>
                    {{ $site_name }}<br />
                    @if(!empty($team->address)){{ $team->address }}<br />@endif
                    @if(!empty($team->address_2)){{ $team->address_2 }}<br />@endif
                    @if(!empty($team->city)){{ $team->city }}, {{ $team->state }} {{ $team->zip }}<br />@endif
                    @if(!empty($team->phone)){{ $team->phone }}<br />@endif
                </p>
                @if(!empty($map))
                    <div class="mapouter"><div class="gmap_canvas"><iframe width="100%" height="250" id="gmap_canvas" src="https://maps.google.com/maps?q={{ $map }}&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div><style>.mapouter{text-align:right;height:250px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:250px;width:100%;}</style></div>
                @endif
            </div>
            @endif
            <!-- Column -->
            @if(!empty($sign_up) && $website->newsletter_signup == true)
            <div class="column has-text-centered">
                @if(empty($thanks))
                    <h1 class="title">Sign up for our Newsletter!</h1>
                    <p>{!! $sign_up !!}</p>
                @endif
            </div>
            @endif
            <!-- Column -->
            <div class="column has-text-centered">
                @if(!empty($footer['menu_items']))
                <h1 class="title">Menu</h1>
                <ul class="footer-column">
                    @foreach ($footer['menu_items'] as $item)
                    <li><a href="{{ $item['link'] }}" {!! strpos($item['link'], 'http') === false ? '' : 'target="_blank"' !!}>{{ $item['title'] }}</a></li>
                    @endforeach
                </ul>
                <br />
                @endif
                @if(!empty($social))
                <h1 class="title">Social</h1>
					@if(!empty($team->facebook))<a title="Facebook" href="{{ $team->facebook}}" target="_blank" class="button is-rounded"><i class="fa fa-facebook"></i></a>@endif
					@if(!empty($team->twitter))<a title="Twitter" href="{{ $team->twitter}}" target="_blank" class="button is-rounded"><i class="fa fa-twitter"></i></a>@endif
					@if(!empty($team->instagram))<a title="Instagram" href="{{ $team->instagram}}" target="_blank" class="button is-rounded"><i class="fa fa-instagram"></i></a>@endif
					@if(!empty($team->youtube))<a title="Youtube" href="{{ $team->youtube}}" target="_blank" class="button is-rounded"><i class="fa fa-youtube"></i></a>@endif
					@if(!empty($team->linkedin))<a title="LinkedIn" href="{{ $team->linkedin}}" target="_blank" class="button is-rounded"><i class="fa fa-linkedin"></i></a>@endif
					@if(!empty($team->yelp))<a title="yelp" href="{{ $team->yelp}}" target="_blank" class="button is-rounded"><i class="fa fa-yelp"></i></a>@endif
                @endif
            </div>
        </div>
        <div class="columns has-text-centered">
            <div class="column is-12">
                &copy; {{ $year }} All Rights Reserved.
                @if($website->powered_by)
                    <br />
                    <a href="https://dataczar.com/" target="_blank" class="button" style="background-color: #fff; margin-top: 15px;" title="Powered by Dataczar">
                        <img src="{{ asset('img/dataczar-power-logo.png') }}" style="width:125px;padding:5px;">
                    </a>
                @endif
            </div>
        </div>
    </div>
</div>
    
<script type="text/javascript">
	document.addEventListener("DOMContentLoaded", () => {
		// Get all "navbar-burger" elements
		const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll(".navbar-burger"), 0);
		// Check if there are any navbar burgers
		if ($navbarBurgers.length > 0) {
			// Add a click event on each of them
			$navbarBurgers.forEach( el => {
				el.addEventListener("click", () => {
					// Get the target from the "data-target" attribute
					const target = el.dataset.target;
					const $target = document.getElementById(target);
					// Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
					el.classList.toggle("is-active");
					$target.classList.toggle("is-active");
				});
			});
		}
	});
</script>

</body>

@endsection

