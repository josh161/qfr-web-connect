<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        @includeif('templates.website.general.meta')
		<title>{{ $site_name }}{{ !empty($post->title) ? " - $post->title" : '' }}</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.8.1/css/bulma.min.css">
        @yield('template_css')
        @includeif('templates.website.general.pixels')
        {!! $website->scripts ?? '' !!}
        {!! $post->scripts ?? '' !!}
	</head>
	@yield('template_body')
</html>