@if(isset($post->settings->gallery_status) 
    && $post->settings->gallery_status == 'enabled' 
    && isset($post->settings->gallery)
    && count($post->settings->gallery) > 0)
        <script>var gallery = {!! str_replace('\\', '', json_encode($post->settings->gallery)) !!}</script> 
        <div id="app"></div>
        <script src="https://trk.dzr.io/src/slider/v1/js/app.a9dc9cc3.js"></script>
        <script src="https://trk.dzr.io/src/slider/v1/js/chunk-vendors.e8e0b2a3.js"></script>
@endif